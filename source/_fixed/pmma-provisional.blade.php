@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'PMMA Provisionals',
    'meta_description' => 'Our team mills our provisionals out of a dense block of PMMA acrylic. They offer durability and temporary esthetics.'
    ])
@endsection

@section('body')
<section id="product-page">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-4">
                <img src="/img/Temporaries.png" alt="PMMA Provisionals thumbnail">
            </div>
            <div class="col-sm-12 col-md-8">
                <h1>PMMA Provisionals</h1>
                <p>Our team mills our provisionals out of a dense block of PMMA acrylic. They offer durability and temporary esthetics. PMMA is naturally translucent, so your patient can wait for their final restoration with confidence. We recommend our PMMA provisionals for any patient who requires a temporary solution.</p>
                <p><a href="/send-case/new-doctor" class="btn">Prescribe Today!</a></p>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection