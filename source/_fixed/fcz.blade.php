@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Full-Contour Zirconia',
    'meta_description' => 'The full-contour zirconia restorations from United are crafted with state-of-the-art technology. They offer the highest flexural strength of any all-ceramic.'
    ])
@endsection

@section('body')
<section id="product-page">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-4">
               <img src="/img/FullContourZirconia.png" alt="Full-Contour Zirconia thumbnail">
                <div class="row">
                    <div class="col-sm-12 col-md-6" style="text-align: center;">
                        <img src="/img/simply_anterior.png" alt="Simply Z anterior thumbnail">
                    </div>
                    <div class="col-sm-12 col-md-6" style="text-align: center;">
                        <img src="/img/simply_posterior.png" alt="Simply Z posterior thumbnail">
                    </div>
                    <div class="col-12">
                        <img src="/img/advantage_posterior.png" alt="Advantage Z thumbnail">
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-8">
                <h2>Full-Contour Zirconia</h2>
                <p>The full-contour zirconia restorations from United are crafted with state-of-the-art technology. They offer the highest flexural strength of any all-ceramic restoration. Our technicians ensure every full-contour zirconia crown or bridge is crafted with a perfectly smooth surface. You can prescribe with confidence knowing that these restorations will not have any abrasive surfaces that could potential harm opposing dentition. Full-contour zirconia is a metal-free solution that is 100% biocompatible and an excellent product for bruxing patients. We offer our exceptional Advantage Z and Simply Z full-contour zirconia options that are only available at United Dental Lab. </p>
                <h3>Simply Z</h3>
                <p>Our Simply Z restorations fulfill the needs of patients, whether they are looking for an anterior or posterior solution. Fabricated as a monolithic crown, Simply Z provides superior translucency and vitality. Alongside its lifelike, anterior esthetics, it also provides extreme strength that is suitable for posterior cases.</p>
                <h3>Advantage Z</h3>
                <p>This zirconia is ideal for the posterior due to its durability and fracture-resistance. The natural translucency of this zirconia ensures an esthetic solution that will blend in with surrounding dentition. Give your practice the advantage of a high-quality zirconia that delivers on strength, esthetics, and price. </p>
                <p><a href="/send-case/new-doctor" class="btn">Prescribe Today!</a></p>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div id="accordion">
                    <h3>Indications</h3>
                    <div>
                        <p>Full-contour zirconia is so versatile, it can be used in almost any situation from singles, bridges with any combination of abutments and pontics, inlay bridges and screw-retained implants. Also an esthetic alternative to a PFM with metal occlusion due to limited space.</p>
                    </div>
                    <h3>Contraindications</h3>
                    <div>
                        <p>When esthetic expectations are high and it is important that the restorations match surrounding natural dentition or other existing restorations. If bonding is necessary to retain the restoration, bond strength is weaker and less predictable than other ceramics.</p>
                    </div>
                    <h3>Preparation</h3>
                    <div>
                        <p>Shoulder preparation not needed. A mild chamfer or a feather-edge margin is good. 1mm buccal, lingual, and occlusal reduction is ideal, but can go to .5mm in some areas when reduction is limited. Minimum occlusal reduction of 0.5 mm; 1 mm is ideal. Adjustments and polishing: Adjust full-contour zirconia crowns and bridges using water and air spray to keep the restoration cool and to avoid microfractures with a fine grit diamond. If using air only, use the lightest touch possible when making adjustments. A football-shaped bur is the most effective for occlusal and lingual surfaces (on anterior teeth); a tapered bur is the ideal choice for buccal and lingual surfaces. Polish full-contour zirconia restorations with the porcelain polishing system of your choice.</p>
                    </div>
                    <h3>Cementation</h3>
                    <div>
                        <p>It is recommended that full-contour zirconia be cemented using a zirconia primer like Z-Prime from Bisco or Clearfil Ceramic Primer from Kuraray. Alternatively, a resin reinforced glass ionomer such as RelyX Luting cement can also be used. When a greater bond is needed due to the lack of a retentive preparation, use a resin cement like RelyX Unicam or RelyX Ultimate. Before cementing all full-contour zirconia crowns, the interior surface of the crown needs to be cleaned with Ivoclean (Ivoclar Vivadent - Amherst, NY). This is critical in assuring maximum bond strength.</p>
                    </div>
                    <h3>Tech Notes</h3>
                    <div>
                        <p>Advantage Z has a flexural strength of 700-750 MPa, making it an excellent choice for the posterior.</p>
                        <p>Simple Z has a flexural strength of 750-800 MPa, making it an excellent choice for both the anterior and posterior. </p>
                        <p>MPa (Megapascal) is the unit by which the amount of pressure an object can withstand is measured. A natural tooth’s flexural strength is measured at 305 MPa for a molar and 248 MPa for a premolar.</p> 
                    </div>
                    <h3>Codes</h3>
                    <div>
                        <ul>
                            <li>D2740 Crown – Porcelain/Ceramic Substrate</li>
                            <li>D6245 Pontic Porcelain/Ceramic</li>
                            <li>D6740 Abutment Crown Porcelain/Ceramic</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection