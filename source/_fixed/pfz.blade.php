@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'PFZ',
    'meta_description' => 'Porcelain-fused-to-zirconia restorations combine the translucent esthetics of porcelain with the exceptional strength of zirconia for a durable anterior solution.'
    ])
@endsection

@section('body')
<section id="product-page">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-4">
                <img src="/img/IPS-emax-Anterior.png" alt="IPS Emax thumbnail">
                <img src="/img/simply_layered.png" alt="Simply Z Layered" style="padding-bottom: 15px;">
            </div>
            <div class="col-sm-12 col-md-8">
                <h1>PFZ</h1>
                <p>United Dental Lab’s porcelain-fused-to-zirconia restorations are available as our high-quality Simply Z – Layered solution. Simply Z – Layered combines the translucent esthetics of porcelain with the exceptional strength of zirconia for a long-lasting anterior solution. This PFZ is highly customizable and is available either with fully-layered porcelain or with a micro cutback on the facial surface. The practically indestructible zirconia substructure has a high flexural strength of up to 1,450 MPa. These restorations are hypoallergenic and biocompatible. It is a preferable alternative to PFM restorations due to its high esthetics and elimination of common esthetic challenges that are typical of metal-based products. These issues include discoloration and a dark line that appears along the gingiva. These crowns have a warmer, lifelike coloration when compared to their PFM counterparts.</p>
                <p><a href="/send-case/new-doctor" class="btn">Prescribe Today!</a></p>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div id="accordion">
                    <h3>Indications</h3>
                        <div>
                            <p>A CAD/CAM substitute for traditional PFM, our porcelain-fused-to-zirconia can be used for anterior and posterior crowns, crowns over implants, and bridges of up to fourteen units.</p>
                        </div>
                        <h3>Contraindications</h3>
                        <div>
                            <ul>
                                <li>Attachment cases</li>
                                <li>Cases with less than 1 mm clearance</li>
                                <li>Bruxism</li>
                                <li>Patients who have broken a PFM crown</li>
                                <li>Cases that require bonding</li>
                            </ul>
                        </div>
                        <h3>Preparation</h3>
                        <div>
                            <p>The ideal preparation for PFZs is a chamfer margin preparation. If a porcelain labial margin is prescribed, then a shoulder margin preparation is required.</p>
                        </div>
                        <h3>Cementation</h3>
                        <div>
                            <ul>
                                <li>Resin Ionomer cement (RelyX or RelyX Unicem, 3M ESPE)</li>
                                <li>Maxcem Elite (Kerr)</li>
                                <li>Panavia F 2.0 (Kuraray) - ideal for short, tapered preparations</li>
                                <li>Glass ionomer cement (GC Fuji, GC America)</li>
                            </ul>
                        </div>
                        <h3>Tech Notes</h3>
                        <div>
                            <p>If an adjustment is required on the ceramic, use a fine diamond with water and air to keep the crown cool. To contour the ceramic, polish with a pink rubber wheel and diamond polishing paste (Brasseler, Shofu, Vident).</p>
                        </div>
                        <h3>Codes</h3>
                        <div>
                            <ul>
                                <li>D2740 Crown - porcelain / ceramic substrate</li>
                            </ul>
                        </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection