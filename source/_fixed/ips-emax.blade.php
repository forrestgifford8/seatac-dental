@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'IPS e.max®',
    'meta_description' => 'IPS e.max® is the preferred restoration for patients who desire a highly esthetic all-ceramic solution.'
    ])
@endsection

@section('body')
<section id="product-page">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-4">
                <img src="/img/IPS-emax-Anterior.png" alt="IPS Emax thumbnail">
            </div>
            <div class="col-sm-12 col-md-8">
                <h1>IPS e.max®</h1>
                <p>IPS e.max® is the preferred restoration for patients who desire a highly esthetic all-ceramic solution. This exceptional product provides lifelike esthetics with four levels of translucency. IPS e.max® is crafted out of the revolutionary lithium disilicate glass-ceramic. It is highly customizable as it can be fabricated as a full-contour monolithic or cut-back and layered with porcelain for unparalleled anterior beauty. IPS e.max® is fracture-resistant, 2.5 to 3 times stronger than its counterparts, and has a flexural strength of up to 500 MPa. This all-ceramic also has additional impulse ingots for maximum flexibility. We recommend this product to any clinician who wants to prescribe exceptional beauty, longevity, and durability.</p>
                <p><a href="/send-case/new-doctor" class="btn">Prescribe Today!</a></p>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div id="accordion">
                    <h3>Indications</h3>
                    <div>
                        <p>IPS e.max is the premium lithium disilicate, glass-ceramic restoration. Combining lifelike materials and fracture resistant properties, IPS e.max is as durable as it is lifelike. The flexibility of IPS e.max makes it an excellent restorative method for anterior esthetics or posterior function.</p>
                    </div>
                    <h3>Contraindications</h3>
                    <div>
                        <p>Bridges which include molars, Maryland style bridges, and bridges which have a short vertical height that does not allow for adequate connector height.</p>
                    </div>
                    <h3>Preparation</h3>
                    <div>
                        <p>Anterior full-coverage crowns require a chamfer or shoulder margin. A circular shoulder is prepared with rounded inner edges or a chamfer at an angle of 10-30°: the width of the shoulder/chamfer is approx. 1 mm. Facial reduction is 1.5 – 2 mm; 1 – 1.5 mm lingual contact clearance. Incisal reduction is 1.5 – 2 mm with rounded internal line angles, and an incisal edge at least 1mm wide to permit optimum milling of the incisal edge during CAD/CAM processing.</p>

                        <p>Posterior full-coverage crown requires a chamfer or shoulder margin. A circular shoulder is prepared with rounded inner edges or a chamfer at an angle of 10-30°: the width of the shoulder/chamfer is approx. 1 mm. Occlusal reduction is 1.5 – 2 mm: axial reduction (buccal, lingual, and interproximal) is 1.5 mm with rounded internal line angles.</p>
                    </div>
                    <h3>Cementation</h3>
                    <div>
                        <p>IPS e.max layered – can be cemented using a resin reinforced glass ionimer such as RelyX Luting cement. Or bonded using a resin cement when extra strength is needed due to lack of retention on the prep, use a resin cement such as RelyX Unicem or RelyX Ultimate.</p>
                    </div>
                    <h3>Tech Notes</h3>
                    <div>
                        <p>If adjustments are needed, use fine diamonds with water and light pressure. Always remove the crown when adjusting or bond/cement crown before adjustments are made.</p>
                    </div>
                    <h3>Codes</h3>
                    <div>
                        <ul>
                            <li>D2740 Crown</li>
                            <li>D2610 Inlay for 1 surface</li>
                            <li>D2620 Inlay for 2 surfaces</li>
                            <li>D2630 Inlay for 3 surfaces</li>
                            <li>D2962 Labial Veneer</li>
                            <li>D2783 Crown 3/4 Porcelain Ceramic (does not include veneers)</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection