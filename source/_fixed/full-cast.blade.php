@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Full Cast',
    'meta_description' => 'Full cast crowns have a long track record of success and still serve as a dependable solution for the posterior region.'
    ])
@endsection

@section('body')
<section id="product-page">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-4">
                <img src="/img/FullCast_PosteriorCrown.png" alt="Full Cast thumbnail">
            </div>
            <div class="col-sm-12 col-md-8">
                <h1>Full Cast</h1>
                <p>Full cast crowns have a long track record of success and still serve as a dependable solution for the posterior region. Their clinical success is based on their biocompatibility, convenience, and longevity. Our team uses the latest equipment to ensure this industry staple provides precision and consistent fit. We also always ensure these crowns do not have an abrasive finish, which could be harmful to opposing dentition. Full cast restorations can last a lifetime with proper cleaning and maintenance.</p>
                <p><a href="/send-case/new-doctor" class="btn">Prescribe Today!</a></p>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div id="accordion">
                    <h3>Indications</h3>
                    <div>
                        <p>Full-cast gold crowns are indicated for crowns, veneers, inlays, onlays, and bridges.</p>
                    </div>
                    <h3>Contraindications</h3>
                    <div>
                        <p>Full-cast gold crowns are contraindicated for partials and implants.</p>
                    </div>
                    <h3>Preparation</h3>
                    <div>
                        <p>Inlays and onlays can also be fabricated as a full-cast restoration. Feather-edge margin preparations are indicated for full-cast restorations, but any margin preparation may be used.</p>
                    </div>
                    <h3>Cementation</h3>
                    <div>
                        <ul>
                            <li>Panavia 21 (Must be tin plated if precious metal is used)</li>
                            <li>Glass ionomer cement (GC Fuji, GC America)</li>
                            <li>Zinc Phosphate Polycarboxylate Resin Ionomer cement (RelyX, 3M ESPE)</li>
                        </ul>
                    </div>
                    <h3>Tech Notes</h3>
                    <div>
                        <div class="row">
                            <div class="col-12 col-sm-6">
                                <p>All castings are made with a metal alloy, be it non-precious, semi-precious or precious metals. Alloys are classified by their content.</p>
                                <ul>
                                    <li>Base – contents include non-precious, Chrome Cobalt or Titanium</li>
                                    <li>Noble – consists of 25 percent precious alloy</li>
                                    <li>High Noble – consists of 60 percent precious metal with at least 40 percent being gold</li>
                                </ul>
                            </div>
                            <div class="col-12 col-sm-6">
                                <p>Alloy type refers to the hardness and/or softness of the material.</p>
                                <ul>
                                    <li>Type I – Extra soft</li>
                                    <li>Type II – Soft</li>
                                    <li>Type III– Hard</li>
                                    <li>Type IV – Extra Hard (Rigid)</li>
                                    <li>Non-Precious, Noble 20, White High Noble – Type IV – Very hard and rigid. These crowns are more difficult to adjust and re-polish than alloys with a high gold content.</li>
                                    <li>Full Cast 40 – Type III – Yellow high noble alloy. Brand name currently used is Argenco 40 HN.</li>
                                    <li>Full Cast 52 HN – Type III – Yellow high noble alloy. Brand name currently used is Argenco 52.</li>
                                    <li>Full Cast 75- Type III – Yellow high noble and is an upgrade from full cast 52. The gold is slightly more yellow in color. Brand name currently used Argenco 75.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <h3>Codes</h3>
                    <div>
                        <ul>
                            <li>D2790 Crown Full-Cast High Noble Metal</li>
                            <li>D2791 Crown Full-Cast Predominantly Base Metal</li>
                            <li>D2792 Crown Full-Cast Noble Metal</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection