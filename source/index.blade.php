@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Unlimited Quality for Every Case',
    'meta_description' => 'Based in Austin, Texas, Seatac Dental Lab is your resource for high-quality restorations and patient satisfaction-focused personal service.'
    ])
@endsection

@section('body')
    <img id="home-blue-circle" src="/img/Logo-Mark-Blue.png" alt="Home Blue Circle">
    <div id="home-banner">
        <div class="container">
            <div id="home-banner-img" class="row">
                <div class="col-12">
                    <h1>The Laboratory of Choice for Dental Practices</h1> 
                </div>
            </div>
            <div id="home-banner-btns" class="row">
                <div class="col-12 col-md-6">
                    <a href="/send-case/new-doctor/" class="btn-white">Send a Case</a>
                </div>
                <div class="col-12 col-md-6">
                    <a href="/contact/" class="btn-white">The Seatac Difference</a>
                </div>
            </div>
        </div>
    </div>
    <div id="home-cta">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-7">
                    <h1>Unlimited Quality for Every Case</h1>
                    <p>Seatac Dental Lab is your full-service resource for high-quality restorations and patient satisfaction-focused personal service. Our team of motivated dental experts prioritize your practice's success and make it a point to always be available whenever a need arises. Through the use of cutting-edge technology and the latest materials and processes, we are able to provide the absolute best in restorative products and services. We offer a full line of fixed, removables, implants, and value-added services. Stop looking for the silver lining on difficult to work with laboratories and embrace the limitless benefits that accompany a partnership with Seatac Dental Lab. </p>
                    <a href="/about-us/" class="btn">Learn More</a>
                </div>
                <div class="col-sm-12 col-md-5" style="text-align: center;">
                    <img src="/img/DentistInOffice@2x.png" alt="Unlimited Quality Thumnail">
                </div>
            </div>
        </div>
    </div>
    <div id="home-products">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-4">
                    <div class="home-prod-cta">
                        <img src="/img/Homepage_ProductCollage_fixed.png" alt="Fixed Products"> 
                        <p>Our traditional and innovative fixed restorations will fulfill your patient's every need for durability and esthetics. </p>
                        <p><a href="/products/fixed/" class="btn-white">Fixed</a></p>
                    </div>
                </div>
                <div class="col-sm-12 col-md-4">
                    <div class="home-prod-cta">
                        <img src="/img/Homepage_ProductCollage_removables@2x.png" alt="Removables"> 
                        <p>Rejuvenate the confidence of your fully and partially edentulous patients with our expertly crafted removable prosthetics.</p>
                        <p><a href="/products/removables/" class="btn-white">Removables</a></p>
                    </div>
                </div>
                <div class="col-sm-12 col-md-4">
                    <div class="home-prod-cta">
                        <img src="/img/Homepage_ProductCollage_implants.png" alt="Implants"> 
                        <p>Our abutments are available to fit a wide variety of implant manufacturers and will help ensure successful surgeries. </p>
                        <p><a href="/products/implants/" class="btn-white">Implants</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="home-cta-2">
        <img id="home-black-circle" src="/img/Black-Mark.png" alt="Home Circle mark 2">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-5" style="text-align: center;">
                    <img src="/img/Full-Service-Send-A-Case-Dark-Box@2x.png" alt="Unlimited Quality Thumnail">
                </div>
                <div class="col-sm-12 col-md-7">
                    <h1>Get Started Today!</h1>
                    <p>Sending your case to Seatac Dental Lab is easy and convenient, whether it is your first submission or your 100th time working with our team. We have streamlined our process, so you never have to waste time finding all the resources. Everything you need is located on our website in one area, including Rx forms, shipping labels, and more. We invite you to send us your case today and discover the stress-free partnership of Seatac.</p>
                    <a href="/send-case/new-doctor/" class="btn">Learn More</a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection