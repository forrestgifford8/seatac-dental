<!-- Include Jquery UI JS and CSS for Datepicker Function -->
<script src="/jquery-js/jquery-ui.js"></script>
<link rel="stylesheet" href="/jquery-css/jquery-ui.css">

<!-- Form -->
<div id="case-calendar-wrap">
    <div>
        <form id="caseCal-Form" method="post" name="CalForm" action="" id="FORM">
            <script>
                jQuery(function($) {
                    $(document).ready(function(){
                        $( "#datepicker" ).datepicker();
                    });
                });
            </script>
            <label for="dateSelected">Select a Date the Case is Shipped or Picked Up: </label><br>
            <input name="dateSelected" type="text" id="datepicker" placeholder="Click to Select"><br>

            <label for="products" style="margin-top: 10px;">Select a Product:</label><br>
            <select name="product" id="product" style="max-width: 250px;" placeholder="Select a Product">
                <option value="">Click to select...</option>
                <option value="">Crown Restorations</option>
                <option value="5">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• Zirconia - Anterior or Posterior</option>
                <option value="7">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• SimplyZ Layered</option>
                <option value="6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• IPS e.Max</option>
                <option value="7">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• PFM's</option>
                <option value="4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• Full Cast Crowns</option>
                <option value="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• Diagnostic Wax-up</option>
                <option value=""><b>Temps</b></option>
                <option value="7">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• Acrylic - CAD</option>
                <option value="">Removables</option>
                <option value="5">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• Partial Framework Only</option>
                <option value="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• Partial Set-Up</option>
                <option value="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• Partial Finish</option>
                <option value="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• Partial Repair/Addition</option>
                <option value="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• Flexible Partial Set-Up</option>
                <option value="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• Flexible Partial Finish</option>
                <option value="1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• Custom Tray</option>
                <option value="1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• Occlusion Rim</option>
                <option value="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• Denture Set-Up</option>
                <option value="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• Denture Reset</option>
                <option value="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• Denture Finish</option>
                <option value="1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• Soft Clasp</option>
                <option value="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• Duplicate Denture</option>
                <option value="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• Flipper</option>
                <option value="4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• Implant Surgical Guide</option>
                <option value="4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• Bite Splint - Hard</option>
                <option value="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• Comfort H/S Bite Splint</option>
                <option value="1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• Repairs - Simple (1-3 Teeth)</option>
                <option value="1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• Reline</option>
                <option value="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• Soft Reline</option>
                <option value="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;• Jump Rebase Denture</option>
            </select><br>

            <!-- <label for="modelOrStl" style="margin-top: 10px;">Model or STL:</label>
            <input type="radio" name="modelOrStl" value="model"> Model
            <input type="radio" name="modelOrStl" value="stl"> STL 
            <br> -->

            <input class="cal_submit btn" type="submit" name="button" id="button" value="Submit" style="margin-top: 10px;" data-url="/posts/caseCalendar.php">
        </form>
    </div>
    <div id="cal-result-column">
        <div id="temp" class="result-wrapper"></div>
    </div>
</div>
<!-- Validate Form Entries -->
<script type="text/javascript">
    function validateForm_Cal() {
        
        if ($("#datepicker").val() ==  null || $("#datepicker").val() == "" ) {
            window.alert("Please Select a Date");
            return false;
        } else if ($("#product").val() == ""|| $("#product").val() == null) {
            window.alert("Please Select a Product");
            return false;
        } else {
            return true;
        }
        //}else if(modelOrStl==null || modelOrStl==""){
            //alert("Please Select either Model or STL");
            //return false; 
        //}else if((modelOrStl=="stl" && product=="10") || modelOrStl=="stl" && product=="7"){
            //alert("Sorry, but Pearl Full Contour Zirconia Bridge and Pricision Milled bars must be sent by traditional model.");
            //return false;
    }
    jQuery(function($) {
        $(document).ready(function(){
            $("#caseCal-Form").submit(function(event){
                
                var calURL = jQuery('.cal_submit').data('url');
                
                event.preventDefault();
                if( validateForm_Cal() == false ){
                    return;
                } 
                
                $.ajax({
                    type: 'POST',
                    url: calURL,
                    data: {
                        dateSelected: $("#datepicker").val(),
                        product: $("#product").val()
                    },
                    success: function(data) {
                        $(document).find("#temp").empty();
                        $(document).find("#temp").append(data);
                    },
                    error: function(data) {
                        console.log("FAIL");
                    }
                });
            });
        });
    });
</script>

<style type="text/css">
    #caseCal-Form {
        
    }
    .cal_key {
        float: left;
        width: auto;
    }
    .result-wrapper {
        display: grid;
        grid-template-columns: repeat(1, 1fr);
        padding-top: 15px;
    }
    #cal-result-column {
       grid-column: span 3; 
    }
    #case-calendar-wrap {
        float:left;
        padding-bottom:25px;
        display: grid;
        grid-gap: 2em;
        grid-template-columns: repeat(1, 1fr);
    }
    .scb-shipping-day{
        background: #A9D1FE;
    }
    .scb-processing-day{
        background: #C0FEBA;
    }
    .scb-in-lab-processing-day{
        background: #d7d8a2;
    }
    .scb-estimated-delivery-day{
        background: #FE9794;
    }
    .scb-calendar-results {
        width: 300px;
        display: none;
        background: #bbb;
        padding: 10px;
    }
    .scb-calender-month {
        background: #798b90 none repeat scroll 0 0;
        color: #fff;
        text-align: center;
        border-top: 1px solid #000;
    }
    .scb-calender-head {
        background: gray none repeat scroll 0 0;
        color: #000;
    }
    .scb-calender th,
    .scb-calender td{
        padding: 6px 10px;
        border: none;	
    }
    .scb-calender {
        background: #ccc none repeat scroll 0 0;
        border-bottom: 1px solid #000;
    }
    .scb-holiday-day{
        background: #fcd17d none repeat scroll 0 0;   
    }
    #ui-datepicker-div{
        z-index: 99999999 !important;
    }
    .scb-form-container {
        height: 600px;
        overflow-x: hidden;
    }
    .cal_table {
        border-collapse: collapse;
        border-bottom: 1px solid black;
        border-top: 1px solid black;
        width: 250px;
        margin: auto;
    }
    .cal_table th{
        border: none;
    }
    .month_bkdg {
        background-color:#798B90;
        color:#fff;
        text-align: center;
        font-size: 20px;
        padding: 5px 0px;
    }
    .dayNames{
        background-color:#808080;
        color:#fff; 
        text-align: center;
    }
    .dayNames th{ 
        font-size: 18px;
        padding: 5px 3px;
        min-width: 43px;
    }
    .ship_day{
        background-color:#d7d8a2;
        text-align: center;
    }
    .pre_proc_day {
        background-color:#fe9794;
        text-align: center;
    }
    .proc_day {
        background-color:#A9D1FE;
        text-align: center;
    }
    .holiday {
        background-color:#fcd17d;
        text-align: center;
    }
    .delivery_day {
        background-color:#fe9794;
        text-align: center;
    }
    .deliv_day {
        background-color:#C0FEBA;
        text-align: center;
    }
    .norm_day{
       background-color: #CCCCCC; 
       text-align: center;
    }
    .cal-kay {
        padding-top:5px;
        max-width:220px;
        padding-top: 10px;
    }
    @media screen and (min-width: 576px) { 
        
    } 
    @media screen and (min-width: 768px) { 
        #case-calendar-wrap { grid-template-columns: repeat(4, 1fr); }
    }
    @media screen and (min-width: 992px) { 
        .result-wrapper { grid-template-columns: repeat(2, 1fr); }
    }
</style>