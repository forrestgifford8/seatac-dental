<nav class="primary-nav d-none d-lg-block" id="primary-nav" menu-expanded="false">
    <a href="https://sky.labstar.com/site_inc/account_login.asp?dest=%2Fpages%2Findex%2Easp&bRedirect=1" class="btn" style="margin: 0 0 7px 0;font-size: 14px;float: right;padding: 5px 15px;" target="_blank">Doctor Portal</a>
    <div style="clear:both"></div>
    <ul class="list-unstyled d-lg-flex">
        <li><a href="/about-us"><span class="h-effect"></span><span class="nav-link-txt">About</span></a></li>
        <li>
            <a href=""  data-target="#products-collapse" aria-expanded="false" data-toggle="collapse"><span class="h-effect"></span><span class="nav-link-txt">Products</span><div class="sub-tri"></div></a>
            <ul class="collapse sub-menu" id="products-collapse" data-parent="#primary-nav">
                <li><a href="/products/fixed">Fixed</a></li>
                <li><a href="/products/removables">Removables</a></li>
                <li><a href="/products/implants">Implants</a></li>
                <li><a href="/products/services">Additional Services</a></li>
            </ul> 
        </li>
        <li>
            <a href=""  data-target="#send-case-collapse" aria-expanded="false" data-toggle="collapse"><span class="h-effect"></span><span class="nav-link-txt">Send a Case</span><div class="sub-tri"></div></a>
            <ul class="collapse sub-menu" id="send-case-collapse" data-parent="#primary-nav">
                <li><a href="/send-case/new-doctor">New Dentist</a></li>
                <li><a href="/send-case/digital-case">Digital Protocols</a></li>
                <li><a href="/img/SKY_Dental_Fixed_RX.pdf" target="_blank">Fixed Rx Form</a></li>
                <li><a href="/img/SKY_Dental_Removable_RX.pdf" target="_blank">Removable Rx Form</a></li>
                <li><a href="/send-case/shipping-label">FedEx Shipping Label</a></li>
                <li><a href="/send-case/case-calendar">Case Scheduler</a></li>
            </ul>
        </li>
        <li><a href="/resources"><span class="h-effect"></span><span class="nav-link-txt">Resources</span></a></li>
        <li><a href="/contact"><span class="h-effect"></span><span class="nav-link-txt">Contact Us</span></a></li>
    </ul>
</nav>
<div id="mobile-nav-icon" class="d-block d-lg-none">
    <div>
        <svg id="hamIcon" viewBox="0 0 32 32"><path class="st0" style="fill:#fff" d="M29.4 10H2.6c-.6 0-1-.4-1-1s.4-1 1-1h26.8c.6 0 1 .4 1 1s-.5 1-1 1zM29.4 17H2.6c-.6 0-1-.4-1-1s.4-1 1-1h26.8c.6 0 1 .4 1 1s-.5 1-1 1zM29.4 24H2.6c-.6 0-1-.4-1-1s.4-1 1-1h26.8c.6 0 1 .4 1 1s-.5 1-1 1z"></path></svg>
    </div>
</div>