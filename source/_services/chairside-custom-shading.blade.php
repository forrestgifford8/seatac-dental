@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Chairside Custom Shading',
    'meta_description' => 'Ensure the highest patient satisfaction for every case with our chairside custom shading service.'
    ])
@endsection

@section('body')
<section id="product-page">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-4">
                <img src="/img/shade-matching.png" alt="Shade Matching thumbnail">
            </div>
            <div class="col-sm-12 col-md-8">
                <h1>Chairside Custom Shading</h1>
                <p>Ensure the highest patient satisfaction for every case with our chairside custom shading service. A truly satisfied patient will not only be happy with the final fit of their restoration, they also need to be pleased with how accurately the product matches their natural dentition. One of our skilled technicians will come to your office and perform this service, so your patient can feel safe and comfortable in a familiar environment.</p>
                <p><a href="/contact-us/" class="btn">Schedule Today!</a></p>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection