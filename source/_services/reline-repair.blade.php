@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Reline and Repair',
    'meta_description' => 'United Dental Labs offers our reline and repair service to adapt a patient’s existing prostheses so it perfectly adheres to the changing landscape of their mouth.'
    ])
@endsection

@section('body')
<section id="product-page">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-4">
                <img src="/img/Complete-Denture.png" alt="Reline and Repair thumbnail">
            </div>
            <div class="col-sm-12 col-md-8">
                <h1>Reline &amp; Repair</h1>
                <p>Rapid bone loss is a potential risk every partially or fully edentulous patient is faced with. Frequent adjustments are required on full and partial dentures to ensure continued comfort and fit. United Dental Labs offers our reline and repair service to adapt a patient’s existing prostheses so it perfectly adheres to the changing landscape of their mouth. Our reline and repair service maintains the strength and integrity of the prosthesis’ material.</p>
                <p><a href="/contact-us/" class="btn">Schedule Today!</a></p>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection