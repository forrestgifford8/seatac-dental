@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Contact Us',
    'meta_description' => 'Seatac Dental Lab is your partner, which means we want to know if you have any comments, questions, or concerns.'
    ])
@endsection

@section('body')
@include('_partials.page-header', ['page_title' => 'Contact Us'])
<section id="page-wrap">
    <div class="container">
        <div class="row">
            <div class="col-12 intro-txt">
                <p>Seatac Dental Lab is your partner, which means we want to know if you have any comments, questions, or concerns. We invite you to contact our friendly team and let us know, so we can continue to provide you with dedicated service and high-quality restorations. </p>
            </div>
        </div>
        <div class="row" style="margin-top: 2rem;">
             <div class="col-12 col-md-5">
                <h4>Seatac Dental Lab</h4>
                <p>17101 Ennis Trail <br> 
                Austin, TX 78717</p>
             </div>
             <div class="col-12 col-md-7">
                <form id="contact-form" action="">
                    <div class="form-label-group mb-3" style="margin: auto;">
                        <input id="name-contactform" class="form-control" placeholder="Your Name" required="required" type="text" />
                    </div>
                    <div class="form-label-group mb-3" style="margin: auto;">
                        <input id="practice-name-contactform" class="form-control" placeholder="Practice Name" required="required" type="text" />
                    </div>
                    <div class="form-label-group mb-3" style="margin: auto;">
                        <input id="phone-contactform" class="form-control" placeholder="Phone Number" required="required" type="tel" />
                    </div>
                    <div class="form-label-group mb-3" style="margin: auto;">
                        <input id="email-contactform" class="form-control" placeholder="Email Address" required="required" type="email" />
                    </div>
                    <div class="form-label-group mb-3" style="margin: auto;">
                        <textarea id="message-contactform" class="form-control" placeholder="What can we do for you?"></textarea>
                    </div>
                    <input type="hidden" id="public_id" value="d3c011b95cac444c95541b51dd73bd80" />
                    <div class="g-recaptcha" data-sitekey="6LfQT4gUAAAAAHV6_L-3gL20JKJK4lb0lkBOIjqz"></div>
                    <button class="btn btn-primary mt-3" type="submit">Send Now</button>
                </form>
                <div class="loader">Loading...</div>
             </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        var formPending = false;
        $('#contact-form').submit(function(event) {
            event.preventDefault();
            if (formPending)
                return;
            formPending = true;
            $(this).hide();
            $('#contactForm .alert').remove();
            $('.loader').show();
            $.ajax({
                url: 'https://sheikah.amgservers.com/api/contact/d3c011b95cac444c95541b51dd73bd80/7db81750e97a4024b21906cfcec576df',
                method: 'post',
                data: {
                    'g-recaptcha-response': $('#g-recaptcha-response').eq(0).val(),
                    id: $('#public_id').eq(0).val(),
                    practice: $('#practice-name-contactform').eq(0).val(),
                    name: $('#name-contactform').eq(0).val(),
                    phone: $('#phone-contactform').eq(0).val(),
                    email: $('#email-contactform').eq(0).val(),
                    message: $('#message-contactform').eq(0).val()
                },
                success: function(data) {
                    $('.loader').hide();
                    $('#contact-form').after('<p>Thank you for contacting us! We\'ll get in touch with you as soon as possible!</p>');
                },
                error: function(data, status, err) {
                    $('.loader').hide();
                    $('#contact-form').show();
                    formPending = false;
                    $('#contact-form button[type="submit"]').before('<div class="alert alert-danger" role="alert">Please fill out all of the fields</div>');
                }
            });
        });
    });
</script>
@endsection