@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'FedEx Shipping Label',
    'meta_description' => 'Please provide all necessary information to generate a FedEx shipping label, which will ensure your case gets to our lab quickly and securely.'
    ])
@endsection

@section('body')
@include('_partials.page-header', ['page_title' => 'FedEx Shipping Label'])
<section id="page-wrap">
    <div class="container">
        <div class="row">
            <div class="col-12 intro-txt">
                <p>Please provide all necessary information to generate a FedEx shipping label, which will ensure your case gets to our lab quickly and securely. </p>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                 <div class="form-container">
                     <form class="form-horizontal" id="fedexLabelForm" method="post" action="https://sheikah.amgservers.com/api/label/create/fedex">
                        <fieldset>
                            <div class="row form-group">
                    <!--            <label for="inputName" class="col-lg-2 control-label">Full Name</label>-->
                                <div class="col-lg-12">
                                    <input class="form-control" id="inputName" name="name" placeholder="Full Name" type="text">
                                </div>
                            </div>
                            <div class="row form-group">
                    <!--            <label for="inputAddress1" class="col-lg-2 control-label">Address</label>-->
                                <div class="col-lg-12">
                                    <input class="form-control" id="inputAddress1" name="address" placeholder="Full Address" type="text">
                                </div>
                                <div class="col-lg-6 col-lg-offset-2">
                                    <input class="form-control" id="inputCity" name="city" placeholder="City" type="text">
                                </div>
                                <div class="col-lg-2">
                                    <input class="form-control" id="inputState" name="state" placeholder="State" type="text">
                                </div>
                                <div class="col-lg-4">
                                    <input class="form-control" id="inputZip" name="zip" placeholder="Zip Code" type="text">
                                </div>
                            </div>
                            <div class="row form-group">
                    <!--            <label for="inputPhone" class="col-lg-2 control-label">Phone</label>-->
                                <div class="col-lg-12">
                                    <input class="form-control" id="inputPhone" name="phone" placeholder="Phone Number" type="tel">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-lg-12">
                    <!--                <label for="inputService" class="col-lg-2 control-label">Service</label><br>-->
                                    <select class="form-control" id="inputService" name="service">
                                        <option value="FEDEX_2_DAY" selected="selected">FedEx 2 Day</option>
                                        <option value="STANDARD_OVERNIGHT" selected="selected">Standard Overnight</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-lg-10 col-lg-offset-2">
                                    <input type="hidden" name="id" value="d3c011b95cac444c95541b51dd73bd80">
                                </div>
                            </div>
                        </fieldset>
                        <button type="submit" class="btn btn-primary submit-btn">Create Label</button>
                    </form>
                </div> 
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('#fedexLabelForm').submit(function(e) {
            e.preventDefault();
            var form = $('#fedexLabelForm fieldset');
            form.find('p.danger').remove();
            var mfooter = $('#fedexLabelForm .submit-btn');
            mfooter.empty();
            mfooter.append('Please Wait');
            $.ajax({
                url: 'https://sheikah.amgservers.com/api/label/create/fedex',
                type: 'POST',
                data: $(this).serialize(),
                success: function(data) {
                    form.hide();
                    mfooter.hide();
                    form.after('<a class="btn btn-primary" href="https://sheikah.amgservers.com/api/label/get/'+data.id+'.pdf" target="_blank" download="UPS-Shipping-Label.pdf">Download Shipping Label</a></div><br><div style="display: flex; justify-content: center;"><embed src="https://sheikah.amgservers.com/api/label/get/'+data.id+'.pdf" style="width:100%;min-height:400px;" /></div>');
                    mfooter.empty();
                },
                error: function(data) {
                    form.append('<p class="danger">There was an error creating a shipping label. Please make sure all fields are correctly filled out and try again</p>');
                    mfooter.empty();
                    mfooter.append('Create Label');
                }
            });
        });
    });
</script>
@endsection