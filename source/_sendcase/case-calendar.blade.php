@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Case Scheduler',
    'meta_description' => 'We are happy to provide an easy-to-use case scheduling calendar which will give you the date when you can expect your case to be returned. '
    ])
@endsection

@section('body')
@include('_partials.page-header', ['page_title' => 'Case Scheduler'])
<section id="page-wrap">
    <div class="container">
        <div class="row">
            <div class="col-12 intro-txt">
                <p>We are happy to provide an easy-to-use case scheduling calendar which will give you the date when you can expect your case to be returned. </p>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                 <div class="form-container">
                    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
                    <form id="caseCal-Form" method="post" name="CalForm" action="">
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input class="form-control" id="date-calendarform" placeholder="Select a Ship Date" required="required" type="text" />
                        </div>
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <select class="form-control" name="product" id="product-calendarform" placeholder="Select a Product">
                                <option value="" style="background-color: #f6f6f6;" disabled selected>Select a Product</option>
                                <option value="8">Full-Contour Zirconia</option>
                                <option value="8">IPS e.max®</option>
                                <option value="8">PFZ</option>
                                <option value="8">PFM</option>
                                <option value="8">Full Cast</option>
                                <option value="8">Provisionals</option>
                                <option value="8">Full Denture</option>
                                <option value="8">Valplast® Flexible Partial</option>
                                <option value="8">Cast Partial</option>
                                <option value="8">OEM Custom Abutments</option>
                                <option value="8">Compatible Custom Abutments</option>
                                <option value="8">Diagnostic Wax-Up</option>
                                <option value="8">Custom Shade Matching</option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary">Get Turnaround Time</button>
                    </form>
                    <div class="loader">Loading...</div>
                </div> 
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('#caseCal-Form').submit(function(e) {
            e.preventDefault();
            $('#caseCal-Form ~ .loader').show();
            $('.case-calendar').remove();
            $.ajax({
                method: 'POST',
                url: 'https://sheikah.amgservers.com/api/case-calendar',
                data: {
                    date: $('#date-calendarform').eq(0).val(),
                    product: $('#product-calendarform').eq(0).val(),
                    shiptime: "1",
                    processingtime: "0",
                    holidays: '["2018-11-21", "2018-11-23", "2018-12-26", "2018-12-31"]'
                },
                success: function(data) {
                    $('#caseCal-Form ~ .loader').hide();
                    $('#caseCal-Form').parent('.form-container').after(atob(data.calendar));
                    $('#delivery-disclaimer').show();
                }, 
                error: function() {

                }
            });
        });

        $( "#date-calendarform" ).datepicker();
    });
</script>
@endsection