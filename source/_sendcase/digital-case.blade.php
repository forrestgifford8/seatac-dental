@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Digital Impression Protocols',
    'meta_description' => 'We invite  you to send your case to Seactac Dental Lab, where we keep our focus on stress-free cases and high patient satisfaction.'
    ])
@endsection

@section('body')
@include('_partials.page-header', ['page_title' => 'Digital Impression Protocols'])
<section id="page-wrap">
    <div class="container">
        <div class="row">
            <div class="col-12 intro-txt">
                <p>Seactac Dental Laboratory accepts files from most major intraoral scanners. To ensure submitting your digital file is as easy as possible, our team uses WeTransfer for all file submissions. Simply follow the protocol below to get your digital file to our team right away.  </p>
            </div>
        </div>
        <div class="row">
            <div class="col-12 intro-txt">
                <p>• Visit wetransfer.com • <br> 
                • Add your files • <br> 
                • Send email to seactaccs@seactacdentallab.com • <br> 
                • Include your email and any message necessary • <br> </p>
            </div>
            <div class="col-12 intro-txt">
                <p><a href="https://wetransfer.com/" class="btn" target="_blank">Visit WeTransfer</a></p> 
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection