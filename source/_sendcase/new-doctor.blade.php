@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'New Dentist',
    'meta_description' => 'We invite  you to send your case to Seatac Dental Lab, where we keep our focus on stress-free cases and high patient satisfaction.'
    ])
@endsection

@section('body')
@include('_partials.page-header', ['page_title' => 'New Dentist'])
<section id="page-wrap">
    <div class="container">
        <div class="row">
            <div class="col-12 intro-txt">
                <p>Thank you for choosing Seatac Dental Lab, where we keep our focus on stress-free cases and high patient satisfaction. We want sending your case to be one of the easiest parts of your professional day, which is why we optimized our case submission process. Getting your traditional or digital case on its way only takes a few minutes, because every resource you may need is compiled in one convenient location.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-8 offset-2 justify-content-between d-none d-lg-flex">
                <div class="step-num active" id="step-num1">
                    <span>1</span>
                </div>
                <div class="step-num" id="step-num2">
                    <span>2</span>
                </div>
                <div class="step-num" id="step-num3">
                    <span>3</span>
                </div>
                <div class="step-num" id="step-num4">
                    <span>4</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div id="step-1" class="col-12 case-step">
                <h2>Step 1</h2>
                <h3>Choose your Preferred Type of Case</h3>
                <div class="row">
                    <div class="col-12 col-md-6">
                        <h4>Send a Traditional Case</h4>
                        <p>With our step-by-step process, you can get your traditional cases to our laboratory right away. Rx forms, shipping labels, local pickup request, and a case scheduler are all located within the following steps. </p>
                        <div id="move-to-2" class="btn">Get Started</div>
                    </div>
                    <div class="col-12 col-md-6">
                        <h4>Send a Digital Case</h4>
                        <p>Seatac Dental Lab is committed to staying at the forefront of digital dentistry. We accept scans from most major intraoral scanners. We invite you to experience the Seatac difference yourself today.</p>
                        <a href="/send-case/digital-case/" class="btn">Get Started</a>
                    </div>
                </div> 
            </div>
            <div id="step-2" class="col-12 case-step d-none">
                <h2>Step 2</h2>
                <h3>Download Rx Form</h3>
                <p>We provide a printable Rx form for all your cases. Simply download the form and include a completed version with your traditional impression. </p>
                <a href="/img/Seatac_Dental_Fixed_RX.pdf" class="btn" target="_blank">Fixed Rx Form</a><br>
                <a href="/img/Seatac_Dental_Removable_RX.pdf" class="btn" target="_blank">Removable Rx Form</a><br>
                <div id="move-to-3" class="btn">Continue <i class="fas fa-arrow-right"></i></div>
            </div>
            <div id="step-3" class="col-12 case-step d-none">
                <h2>Step 3</h2>
                <h3>Generate a Shipping Label</h3>
                <p>We want to ensure your case gets to our lab quickly and securely, without causing unnecessary stress to you or your team. We provide free shipping labels to all our clients.</p>
                <div class="form-container">
                     <form class="form-horizontal" id="fedexLabelForm" method="post" action="https://sheikah.amgservers.com/api/label/create/fedex">
                        <fieldset>
                            <div class="row form-group">
                    <!--            <label for="inputName" class="col-lg-2 control-label">Full Name</label>-->
                                <div class="col-lg-12">
                                    <input class="form-control" id="inputName" name="name" placeholder="Full Name" type="text">
                                </div>
                            </div>
                            <div class="row form-group">
                    <!--            <label for="inputAddress1" class="col-lg-2 control-label">Address</label>-->
                                <div class="col-lg-12">
                                    <input class="form-control" id="inputAddress1" name="address" placeholder="Full Address" type="text">
                                </div>
                                <div class="col-lg-6 col-lg-offset-2">
                                    <input class="form-control" id="inputCity" name="city" placeholder="City" type="text">
                                </div>
                                <div class="col-lg-2">
                                    <input class="form-control" id="inputState" name="state" placeholder="State" type="text">
                                </div>
                                <div class="col-lg-4">
                                    <input class="form-control" id="inputZip" name="zip" placeholder="Zip Code" type="text">
                                </div>
                            </div>
                            <div class="row form-group">
                    <!--            <label for="inputPhone" class="col-lg-2 control-label">Phone</label>-->
                                <div class="col-lg-12">
                                    <input class="form-control" id="inputPhone" name="phone" placeholder="Phone Number" type="tel">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-lg-12">
                    <!--                <label for="inputService" class="col-lg-2 control-label">Service</label><br>-->
                                    <select class="form-control" id="inputService" name="service">
                                        <option value="FEDEX_2_DAY" selected="selected">FedEx 2 Day</option>
                                        <option value="STANDARD_OVERNIGHT" selected="selected">Standard Overnight</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-lg-10 col-lg-offset-2">
                                    <input type="hidden" name="id" value="d3c011b95cac444c95541b51dd73bd80">
                                </div>
                            </div>
                        </fieldset>
                        <button type="submit" class="btn btn-primary submit-btn">Create Label</button>
                    </form>
                </div> 
                <div id="move-to-4" class="btn">Continue <i class="fas fa-arrow-right"></i></div>
            </div>
            <div id="step-4" class="col-12 case-step d-none">
                <h2>Step 4</h2>
                <h3>You're Finished!</h3>
                <p>We hope you found this case submission process easy and stress-free. The next time you send your case to Seatac Dental Lab, we invite you to pick and choose the resources you need from the Send a Case menu above. There you can print Rx forms and shipping labels anytime you need.</p>
                <h4>Optional: Schedule Your Case</h4>
                <p>Our team prides ourselves on punctual cases, because an on-time case is the first step in patient satisfaction. We offer a convenient case scheduling calendar, which will calculate when your case will be returned to your practice. Simply provide us with your case information and we will let you know when you can expect your restoration to arrive.</p>
                <a href="/send-case/case-calendar/" class="btn">Schedule Your Case</a>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        
        $('#move-to-2').click(function(){
            $('#step-num1').removeClass('active');
            $('#step-num2').addClass('active');
            $('#step-1').addClass('d-none');
            $('#step-2').removeClass('d-none');
        });
        $('#move-to-3').click(function(){
            $('#step-num2').removeClass('active');
            $('#step-num3').addClass('active');
            $('#step-2').addClass('d-none');
            $('#step-3').removeClass('d-none');
        });
        $('#move-to-4').click(function(){
            $('#step-num3').removeClass('active');
            $('#step-num4').addClass('active');
            $('#step-3').addClass('d-none');
            $('#step-4').removeClass('d-none');
        });
        
        //FedEx Shipping Label
        $('#fedexLabelForm').submit(function(e) {
            e.preventDefault();
            var form = $('#fedexLabelForm fieldset');
            form.find('p.danger').remove();
            var mfooter = $('#fedexLabelForm .submit-btn');
            mfooter.empty();
            mfooter.append('Please Wait');
            $.ajax({
                url: 'https://sheikah.amgservers.com/api/label/create/fedex',
                type: 'POST',
                data: $(this).serialize(),
                success: function(data) {
                    form.hide();
                    mfooter.hide();
                    form.after('<a class="btn btn-primary" href="https://sheikah.amgservers.com/api/label/get/'+data.id+'.pdf" target="_blank" download="UPS-Shipping-Label.pdf">Download Shipping Label</a></div><br><div style="display: flex; justify-content: center;"><embed src="https://sheikah.amgservers.com/api/label/get/'+data.id+'.pdf" style="width:100%;min-height:400px;" /></div>');
                    mfooter.empty();
                },
                error: function(data) {
                    form.append('<p class="danger">There was an error creating a shipping label. Please make sure all fields are correctly filled out and try again</p>');
                    mfooter.empty();
                    mfooter.append('Create Label');
                }
            });
        });
    });
</script>
@endsection