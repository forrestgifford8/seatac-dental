@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Implants',
    'meta_description' => 'Seatac Dental Laboratory provides several options for custom abutments that will work for a wide variety of implant cases.'
    ])
@endsection

@section('body')
@include('_partials.page-header', ['page_title' => 'Implants'])
<section id="page-wrap">
    <div class="container">
        <div class="row">
            <div class="col-12 intro-txt">
                <p>Seatac Dental Laboratory provides several options for custom abutments that will work for a wide variety of implant cases. We invite you to discover the benefits that come with choosing custom abutments from a dental laboratory partner you can trust.</p>
            </div>
        </div>
        <div class="row" style="margin-top: 2rem;">
            <div class="col-sm-12 col-sm-6 col-lg-4">
                <a href="#mod1" rel="modal:open" class="productModal">
                    <div class="product-thumb" style="min-height: 397px;">
                        <img src="/img/Straumann-Abutment.png" alt="OEM Custom Abutments">
                        <h3>OEM Custom Abutments</h3>
                        <span>Does not Void Manufacturer's Warranty</span><br>
                        <span>Ideal Anatomical Emergence Profile</span>
                        <div class="prodOverlay"><span>Learn More</span></div>
                    </div>
                </a>
                <div id="mod1" class="modal">
                    <h3>OEM Custom Abutments</h3>
                    <p>Seatac Dental Lab is proud to provide OEM custom abutments for a wide variety of implant cases. These OEM abutments are available for all major implant manufactures, including Straumann®, Nobel Biocare®, Zimmer®, and more. These abutments provide ideal anatomical emergence profiles and also support the soft tissue. A main benefit of OEM custom abutments is that they will not void the manufacturer warranty.</p>
                </div>
            </div>
            <div class="col-sm-12 col-sm-6 col-lg-4">
                <a href="#mod2" rel="modal:open" class="productModal">
                    <div class="product-thumb">
                        <img src="/img/Custom-Abutment_tiPosterior.png" alt="Compatible Custom Abutments">
                        <h3>Compatible Custom Abutments</h3>
                        <span>Compatible with Variety of Implant Manufacturers</span><br>
                        <span>Expertly Fabricated and Designed </span>
                        <div class="prodOverlay"><span>Learn More</span></div>
                    </div>
                </a>
                <div id="mod2" class="modal">
                    <h3>Compatible Custom Abutments</h3> 
                    <p>Seatac Dental Laboratory offers custom abutments that are compatible with a wide variety of implant manufacturers, including Straumann®, Nobel Biocare®, Zimmer®, and more. We skillfully craft these custom abutments for the anterior and posterior. Unlike stock abutments, our custom abutments will provide anatomical emergence profiles and support the soft tissue.</p>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection