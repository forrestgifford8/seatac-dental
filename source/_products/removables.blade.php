@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Removables',
    'meta_description' => 'The removable prosthetics from Seatac Dental Lab have the capability to completely turn your patients\' lives around, whether they are partially or fully edentulous.'
    ])
@endsection

@section('body')
@include('_partials.page-header', ['page_title' => 'Removables'])
<section id="page-wrap">
    <div class="container">
        <div class="row">
            <div class="col-12 intro-txt">
                <p>The removable prosthetics from Seatac Dental Lab have the capability to completely turn your patients' lives around, whether they are partially or fully edentulous. We invite you to rejuvenate your patients' smiles with our expertly crafted solutions, which prove that Seatac is the limit.</p>
            </div>
        </div>
        <div class="row" style="margin-top: 2rem;">
            <div class="col-sm-12 col-sm-6 col-lg-4">
                <a href="#mod1" rel="modal:open" class="productModal">
                    <div class="product-thumb">
                        <img src="/img/Complete-Denture.png" alt="Full Denture">
                        <h3>Full Denture</h3>
                        <span>Lifelike Function</span><br>
                        <span>Beautiful Esthetics</span>
                        <div class="prodOverlay"><span>Learn More</span></div>
                    </div>
                </a>
                <div id="mod1" class="modal">
                    <h3>Full Denture</h3>
                    <p>Our team carefully fabricates our full dentures to ensure lifelike function and esthetics. Our goal is to provide a prosthetic that is nearly indistinguishable from your patient's lost dentition. Every prosthetic is fabricated with comfort in mind and will perfectly fit into your patient's oral environment. We utilized the latest materials, processes, and equipment to guarantee high-quality fit and function.</p>
                </div>
            </div>
            <div class="col-sm-12 col-sm-6 col-lg-4">
                <a href="#mod2" rel="modal:open" class="productModal">
                    <div class="product-thumb">
                        <img src="/img/ValplastPartial.png" alt="Valplast® Flexible Partial">
                        <h3>Valplast® Flexible Partial</h3>  
                        <span>Unlimited Design Versatility</span><br>
                        <span>High Esthetics</span>
                        <div class="prodOverlay"><span>Learn More</span></div>
                    </div>
                </a>
                <div id="mod2" class="modal">
                    <h3>Valplast® Flexible Partial</h3>
                    <p>Seatac's Valplast® flexible partials are fabricated out of a biocompatible nylon thermoplastic that provide unique physical and esthetic properties. Valplast® allows for unlimited design versatility, so our team can design the prosthetic that will perfectly blend in with your patient's preexisting dentition. There are numerous benefits for your patient, including increased retention, comfort, esthetics, and strength. In addition, the flexible denture material helps reduce chairtime and eliminates invasive procedures.</p>
                </div>
            </div>
            <div class="col-sm-12 col-sm-6 col-lg-4">
                <a href="#mod3" rel="modal:open" class="productModal">
                    <div class="product-thumb">
                        <img src="/img/CastPartial.png" alt="Cast Partial">
                        <h3>Cast Partial</h3>
                        <span>High Longevity</span><br>
                        <span>Lifelike Function</span>
                        <div class="prodOverlay"><span>Learn More</span></div>
                    </div>
                </a>
                <div id="mod3" class="modal">
                    <h3>Cast Partial</h3>
                    <p>The cast partials from Seatac Dental Lab are carefully crafted to meet all of your patients' needs. Crafted out of the latest materials, these partials offer high precision, lifelike function, longevity, and durability. Despite their rigidity, our team fabricates cast partials that ensure patient satisfaction through superior fit.</p>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection