@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Additional Services',
    'meta_description' => 'The Seatac team is available to assist your team through value-added services.'
    ])
@endsection

@section('body')
@include('_partials.page-header', ['page_title' => 'Additional Services'])
<section id="page-wrap">
    <div class="container">
        <div class="row">
            <div class="col-12 intro-txt">
                <p>Ensure patient satisfaction in every case with our additional services. The Seatac team is available to assist your team through value-added services. Never forget that Seatac Dental Laboratory is the partner you can trust and rely on for every case.</p>
            </div>
        </div>
        <div class="row" style="margin-top: 2rem;">
            <div class="col-sm-12 col-sm-6 col-lg-4">
               <a href="#mod1" rel="modal:open" class="productModal">
                    <div class="product-thumb">
                        <img src="/img/Diagnostic-WaxUp.png" alt="Diagnostic Wax-Up">
                        <h3>Diagnostic Wax-Up</h3>
                        <span>Increased Case Acceptance</span><br>
                        <span>Ensures Extreme Precision</span>
                        <div class="prodOverlay"><span>Learn More</span></div>
                    </div> 
               </a>
                <div id="mod1" class="modal">
                    <h3>Diagnostic Wax-Up</h3>
                    <p>Our team is happy to provide diagnostic wax-up models that will allow you to show your patient a preview of the end result of the restoration process. By showing your patient their finished smiled beforehand, you are able to increase patient satisfaction and confidence in the case. The diagnostic wax-up will also assist our team during the fabrication of the final restoration, which helps ensure extreme precision and a final restoration that accurately achieves your specifications.</p>
                </div>
            </div>
            <div class="col-sm-12 col-sm-6 col-lg-4">
                <a href="#mod2" rel="modal:open" class="productModal">
                    <div class="product-thumb">
                        <img src="/img/shade-matching.png" alt="Custom Shade Matching">
                        <h3>Custom Shade Matching</h3> 
                        <span>Perfectly Matching Restorations</span><br>
                        <span>Efficient Process</span>
                        <div class="prodOverlay"><span>Learn More</span></div>
                    </div>
                </a>
                <div id="mod2" class="modal">
                    <h3>Custom Shade Matching</h3>
                    <p>Custom shade matching will help ensure the final restoration perfectly matches your patient's already existing dentition. A mismatched crown can greatly reduce a patient's satisfaction at the end of a case, which is an issue our lab wants to eliminate for your practice. Our team provides comfortable and professional custom shade matching services that are quick and easy for your patient.</p>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection