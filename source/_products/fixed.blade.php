@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Fixed',
    'meta_description' => 'Our full line of fixed restorations are sure to fit whatever case you may have. We offer highly esthetic solutions and durable posterior restorations.'
    ])
@endsection

@section('body')
@include('_partials.page-header', ['page_title' => 'Fixed'])
<section id="page-wrap">
    <div class="container">
        <div class="row">
            <div class="col-12 intro-txt">
                <p>Our full line of fixed restorations are sure to fit whatever case you may have. We offer highly esthetic solutions, such as FCZ, IPS e.max®, and PFZs. We also provide durable posterior restorations like PFMs and full cast options. No matter what your patient is in need of, our team is capable of fabricating a high-quality solution. </p>
            </div>
        </div>
        <div class="row" style="margin-top: 2rem;">
            <div class="col-sm-12 col-sm-6 col-lg-4">
                <a href="#mod1" rel="modal:open" class="productModal">
                    <div class="product-thumb">
                        <img src="/img/FullContourZirconia.png" alt="Full-Contour Zirconia">
                        <h3>Full-Contour Zirconia</h3>
                        <span>Anterior &amp; Posterior</span><br>
                        <span>Lifelike Translucency</span>
                        <div class="prodOverlay"><span>Learn More</span></div>
                    </div>
                </a>
                <div id="mod1" class="modal">
                    <h3>Full-Contour Zirconia</h3>
                    <p>Full-contour zirconia offers the highest flexural strength of any all-ceramic restoration. Thanks to its innovative composition, FCZ is virtually fracture-proof. The Seatac team utilizes digital dentistry to ensure smooth occlusal surfaces that will eliminate potential negative effects from abrasive surfaces on opposing dentition. FCZ restorations are metal-free, 100% biocompatible, and naturally translucent.</p>
                </div>
            </div>
            <div class="col-sm-12 col-sm-6 col-lg-4">
                <a href="#mod3" rel="modal:open" class="productModal">
                    <div class="product-thumb">
                        <img src="/img/IPS-emax-Anterior.png" alt="IPS e.max®">
                        <h3>IPS e.max®</h3>  
                        <span>500 MPa</span><br>
                        <span>Highly Customizable</span>
                        <div class="prodOverlay"><span>Learn More</span></div>
                    </div>
                </a>
                <div id="mod3" class="modal">
                    <h3>IPS e.max®</h3>
                    <p>IPS e.max® is a highly esthetic all-ceramic restoration that is incomparable to any of its counterparts due to four levels of translucency. IPS e.max® is an innovative lithium disilicate glass-ceramic. It offers 2.5 to 3 times more strength than alternative materials and is virtually fracture-resistant. IPS e.max® has additional impulse ingots, which allow for maximum flexibility. Thanks to its high customization, IPS e.max® is available as a full-contour monolithic or cut-back and layered with porcelain for increased anterior esthetics.</p>
                </div>
            </div>
            <div class="col-sm-12 col-sm-6 col-lg-4">
                <a href="#mod2" rel="modal:open" class="productModal">
                    <div class="product-thumb">
                        <img src="/img/PFZ.png" alt="PFZ">
                        <h3>PFZ</h3>
                        <span>Anterior Esthetics</span><br>
                        <span>Hypoallergenic and Biocompatible</span>
                        <div class="prodOverlay"><span>Learn More</span></div>
                    </div>
                </a>
                <div id="mod2" class="modal">
                    <h3>PFZ</h3>
                    <p>Porcelain-fused-to-zirconia restorations offer increased esthetics compared to its porcelain-fused-to-metal alternatives. They are crafted by layering highly esthetic porcelain over an extremely durable zirconia substructure. PFZ provides a generous amount of strength and translucency from the zirconia that will not distract from the porcelain layering. PFZs are hypoallergenic and biocompatible. They also provide a warmer and more lifelike coloration than PFMs. </p>
                </div>
            </div>
            <div class="col-sm-12 col-sm-6 col-lg-4">
                <a href="#mod6" rel="modal:open" class="productModal">
                    <div class="product-thumb">
                        <img src="/img/PFM_3-Unit.png" alt="PFM">
                        <h3>PFM</h3>
                        <span>Stable and Strong</span><br>
                        <span>Biocompatible</span>
                        <div class="prodOverlay"><span>Learn More</span></div>
                    </div>
                </a>
                <div id="mod6" class="modal">
                    <h3>PFM</h3>
                    <p>Porcelain-fused-to-metal restorations are a timeless standard in the dental industry. They are crafted by fusing a layer of porcelain to an underlying layer of high-quality metal alloys. The porcelain is carefully bonded to eliminate unflattering discoloration, which is a common disadvantage with PFMs. The metal alloy substructure provides stability and strength, which makes it a desirable alternative to crowns made from other materials. The porcelain provides lifelike esthetics to the restored tooth. These crowns are still a popular option due to their high rate of clinical success and their biocompatibility that helps ensure continued periodontal health.</p>
                </div>
            </div>
            <div class="col-sm-12 col-sm-6 col-lg-4">
                <a href="#mod4" rel="modal:open" class="productModal">
                    <div class="product-thumb">
                        <img src="/img/FullCast_PosteriorCrown.png" alt="PFM">
                        <h3>Full Cast</h3>
                        <span>Easy to Use</span><br>
                        <span>Biocompatible</span>
                        <div class="prodOverlay"><span>Learn More</span></div>
                    </div>
                </a>
                <div id="mod4" class="modal">
                    <h3>Full Cast</h3> 
                    <p>Full cast crowns from Seatac Dental Lab are a longstanding preferred choice among clinicians due to their high rate of clinical success and ease of use. Our lab uses CAD/CAM technology to ensure high precision, fit and finish for the final restoration. They are durable yet gentle on opposing dentition and ensure ideal fit, form, and function for patients.</p>
                </div>
            </div>
            <div class="col-sm-12 col-sm-6 col-lg-4">
                <a href="#mod5" rel="modal:open" class="productModal">
                    <div class="product-thumb">
                        <img src="/img/Temporaries.png" alt="Provisionals">
                        <h3>Provisionals</h3>
                        <span>Durable</span><br>
                        <span>Esthetic</span>
                        <div class="prodOverlay"><span>Learn More</span></div>
                    </div>
                </a>
                <div id="mod5" class="modal">
                    <h3>Provisionals</h3>
                    <p>Our Provisionals offer durability and esthetics for a temporary restorative solution. Milled out of a dense block of PMMA, these provisionals provide natural esthetics due to the material's translucency. Available in 16 VITA® classical and bleached shades in both monocolor and multilayer options.</p>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection