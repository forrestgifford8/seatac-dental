@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Page Not Found',
    'meta_description' => 'Error 404. Page not found.'
    ])
@endsection

@section('body')
@include('_partials.page-header', ['page_title' => 'Page Not Found'])
<section id="page-wrap">
    <div class="container">
        <div class="row">
            <div class="col-12 intro-txt" style="text-center">
                <h2>404 - Page Not Found</h2>
                <p>The page you have requested could not be found.</p>
                <a href="/#" class="btn">Home Page</a>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')

@endsection