@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Full Denture',
    'meta_description' => 'We offer a highly esthetic full denture that can met the economic and functional needs of your edentulous patients.'
    ])
@endsection

@section('body')
<section id="product-page">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-4">
                <img src="/img/FullDenture.png" alt="Full Denture thumbnail">
            </div>
            <div class="col-sm-12 col-md-8">
                <h1>Full Denture</h1>
                <p>We offer a highly esthetic full denture that can meet the economic and functional needs of your edentulous patients. These complete prostheses are available in economy and standard variations. The United Team skillfully fabricates our full dentures to offer lifelike esthetics, as well as comfortable fit and function.</p>
                <p><a href="/send-case/new-doctor" class="btn">Prescribe Today!</a></p>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection