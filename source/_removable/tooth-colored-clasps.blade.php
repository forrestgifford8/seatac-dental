@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Tooth-Colored Clasps',
    'meta_description' => 'For added esthetics, United Dental Labs can place tooth-colored clasps on partials. This superior alternative to metal clasps is lightweight.'
    ])
@endsection

@section('body')
<section id="product-page">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-4">
                <img src="/img/Cast-Partial-with-tooth-color-clasps.png" alt="Tooth-Colored Clasps thumbnail">
            </div>
            <div class="col-sm-12 col-md-8">
                <h1>Tooth-Colored Clasps</h1>
                <p>For added esthetics, United Dental Labs can place tooth-colored clasps on partials. This superior alternative to metal clasps is lightweight. These clasps also offer a gentler retentive method that will reduce potential torquing of stabilizing teeth.</p>
                <p><a href="/send-case/new-doctor" class="btn">Prescribe Today!</a></p>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection