@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Cast Partial',
    'meta_description' => 'The cast partials available from United Dental Labs are fabricated out of Vitallium®, a cobalt-chromium alloy'
    ])
@endsection

@section('body')
<section id="product-page">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-4">
                <img src="/img/CastPartial.png" alt="Cast Partial thumbnail">
            </div>
            <div class="col-sm-12 col-md-8">
                <h1>Cast Partial</h1>
                <p>The cast partials available from United Dental Labs are fabricated out of Vitallium®. This cobalt-chromium alloy allows for a strong and durable partial that is also thin and lightweight for improved fit and function. With its superior strength and dependable clasps, this partial can provide your patients with a long-lasting and comfortable solution.</p>
                <p><a href="/send-case/new-doctor" class="btn">Prescribe Today!</a></p>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection