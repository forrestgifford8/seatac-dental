@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Flexible Partial',
    'meta_description' => 'We offer a highly esthetic full denture that can met the economic and functional needs of your edentulous patients.'
    ])
@endsection

@section('body')
<section id="product-page">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-4">
                <img src="/img/Duraflex-Partial-2.png" alt="Flexible Partial thumbnail">
            </div>
            <div class="col-sm-12 col-md-8">
                <h1>Flexible Partial</h1>
                <p>We offer innovative flexible partials that meet the needs of any partially edentulous patient. We are a proud provider of DuraFlex™ flexible partials, which are fabricated out of a revolutionary thermoplastic. This flexible partial resolves the limitations of its predecessors by allowing for easier adjusting and polishing. Prescribe this flexible with confidence as it is clinically unbreakable and more durable than acrylic partials. The DuraFlex™ material is translucent, which increases the presence of natural esthetics. </p>
                <p><a href="/send-case/new-doctor" class="btn">Prescribe Today!</a></p>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection