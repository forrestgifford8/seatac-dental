window.$ = window.jQuery = require('jquery')
require('bootstrap');
require('parsleyjs');
import Swiper from 'swiper';

$(document).ready(function(){
    let lastSwipe = { startX: 0, startY: 0, endX: 0, endY: 0 }
    function closeSidebar() {
        var sidebar = $('body > aside');
        if (sidebar.hasClass('open')) {
            sidebar.removeClass('open');
        }
        $(window).off('click', windowClickListener);
        window.removeEventListener('touchstart', windowTouchStartListener);
        window.removeEventListener('touchend', windowTouchEndListener);
    }
    var windowClickListener = function(e) {
        if (!$(e.target).closest('aside.open').length && !$(e.target).closest('#sidebar-toggle').length)
            closeSidebar();
    }
    var windowTouchStartListener = function(e) {
        lastSwipe.startX = e.changedTouches[0].screenX;
        lastSwipe.startY = e.changedTouches[0].screenY;
    }
    var windowTouchEndListener = function(e) {
        lastSwipe.endX = e.changedTouches[0].screenX;
        lastSwipe.endY = e.changedTouches[0].screenY;
        handleGesture();
    }
    $('#sidebar-toggle').click(function(e) {
        e.preventDefault();
        var sidebar = $('body > aside');
        sidebar.toggleClass('open');
        if (sidebar.hasClass('open')) {
            $(window).on('click', windowClickListener);
            window.addEventListener('touchstart', windowTouchStartListener, false);
            window.addEventListener('touchend', windowTouchEndListener, false); 
        }
    });
    
    function handleGesture() {
        if (lastSwipe.endX <= lastSwipe.startX) {
            var lengthX = Math.abs(lastSwipe.startX - lastSwipe.endX);
            var lengthY = Math.abs(lastSwipe.startY - lastSwipe.endY);
            if (lengthX > lengthY) {
                closeSidebar();
            }
        }
    }
    
    var mySwiper = new Swiper('#swiper-container', {
        speed: 400,
        spaceBetween: 100,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        }
    });
    
    //Mobile Menu
    $("#hamIcon").click(function(){
        $('#mobile-menu').toggleClass('show');
    });
    $("#closeIcon").click(function(){
        $('#mobile-menu').toggleClass('show');
    });

    
    //Accordian
    $( function() {
        $( "#accordion" ).accordion({collapsible: true, active: false});
    } );
});