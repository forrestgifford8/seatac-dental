@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Resources',
    'meta_description' => 'Seatac Dental Lab is your dental partner and resource. We are happy to provide numerous informational resources that will benefit your practice.'
    ])
@endsection

@section('body')
@include('_partials.page-header', ['page_title' => 'Resources'])
<section id="page-wrap">
    <div class="container">
        <div class="row">
            <div class="col-12 intro-txt">
                <p>Seatac Dental Lab is your dental partner and resource. We are happy to provide numerous informational resources that will benefit your practice and ensure every case is stress-free and runs smoothly.  </p>
            </div>
        </div>
        <div class="row" style="margin-top: 2rem;">
             <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                <div class="resource-btn">
                    <a href="/img/Seatac_Dental_Fixed_RX.pdf" target="_blank">
                        <div class="res-bkgd-wrap"><div class="res-bkgd res-rx"></div></div>
                        <h3>Fixed Rx Form</h3>
                        <hr>
                        <div class="btn">Download</div>
                    </a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                <div class="resource-btn">
                    <a href="/img/Seatac_Dental_Removable_RX.pdf" target="_blank">
                        <div class="res-bkgd-wrap"><div class="res-bkgd res-rx"></div></div>
                        <h3>Removable Rx Form</h3>
                        <hr>
                        <div class="btn">Download</div>
                    </a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                <div class="resource-btn">
                    <a href="/img/Torque-Spec-Sheet.pdf" target="_blank">
                        <div class="res-bkgd-wrap"><div class="res-bkgd res-specs"></div></div>
                        <h3>Implant Torque Specs</h3>
                        <hr>
                        <div class="btn">Download</div>
                    </a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                <div class="resource-btn">
                    <a href="/img/Cementation-Guide.pdf" target="_blank">
                        <div class="res-bkgd-wrap"><div class="res-bkgd res-cement"></div></div>
                        <h3>Cementation Guide</h3>
                        <hr>
                        <div class="btn">Download</div>
                    </a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                <div class="resource-btn">
                    <a href="/img/ADA-Code-List.pdf" target="_blank">
                        <div class="res-bkgd-wrap"><div class="res-bkgd res-code"></div></div>
                        <h3>ADA Code List</h3>
                        <hr>
                        <div class="btn">Download</div>
                    </a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                <div class="resource-btn">
                    <a href="/img/DI-Comparison-Chart.pdf" target="_blank">
                        <div class="res-bkgd-wrap"><div class="res-bkgd res-di"></div></div>
                        <h3>DI Comparison Chart</h3>
                        <hr>
                        <div class="btn">Download</div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection