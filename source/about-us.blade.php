@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'About',
    'meta_description' => 'Seatac Dental Lab is your partner, which means we want to know if you have any comments, questions, or concerns.'
    ])
@endsection

@section('body')
@include('_partials.page-header', ['page_title' => 'About'])
<section id="page-wrap">
    <div class="container">
        <div class="row">
            <div class="col-12 intro-txt">
                <p>Seatac Dental Lab was founded in 2004, with the goal to provide stress-free cases that result in high patient satisfaction. Our team is comprised of expert dental laboratory technicians who are capable of producing restorative products that perfectly mimic lifelike esthetics. We are dedicated to ensuring the success of your practice through streamlined and efficient cases that are informed by our own industry insight and expertise in the lab. In addition to their prowess in the lab, our team is happy to sit down with you and discuss the ins and outs of your case. We are your partner through the easy and complex cases, so you can be confident in every restoration you receive from our team. </p>
            </div>
        </div>
        <div class="row">
            <div class="col-12 intro-txt">
                <h2>Digital Dentistry</h2>
                <p>All our high-quality restorations are fabricated with the latest in digital dentistry. We understand the benefits that come from integrating the latest technology into our laboratory, which is why we ensure every case is informed by digital dentistry. Our team uses cutting-edge additive and subtractive technology and CAD/CAM equipment in our lab.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-12 intro-txt">
                <h3>Benefits:</h3>
                <p>• Consistent and High-Quality Restorations • <br> 
                • Reduced Chair Time • <br> 
                • Increased Profitability • <br> 
                • A Streamlined digital workflow • <br> 
                • Prompt and Courteous Communication with Our Lab Staff Members • <br> </p>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')

@endsection