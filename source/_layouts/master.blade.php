<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        @yield('meta')
        <meta name="robots" content="noydir" />
        <meta http-equiv="Cache-Control" CONTENT="no-cache">
        <script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>
        <link rel="icon" type="image/png" href="/img/8251-Favicon_Sky.png">
        <link rel="stylesheet" href="/assets/css/main.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700|Titillium+Web" rel="stylesheet"> 
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
    </head>
    <body class="p-{{ $page->getFilename() }}">
        <header>
            <div class="container">
                <div class="row">
                    <div class="d-flex col-12" id="main-header">
                        <div class="align-self-center">
                            <a href="/">
                                <img id="header-logo" src="/img/Seatac-Dental-Lab-Logo.png" alt="Seatac Dental Lab Header Image">
                            </a>
                        </div>
                        <div>
                            <div class="main-nav-wrap">
                                @include('_components.nav-menu')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="mobile-menu" class="d-sm-block d-md-none">
               <svg id="closeIcon" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 60 60" enable-background="new 0 0 60 60" xml:space="preserve">
                    <g id="X">
                        <line id="_x5C_" fill="none" stroke="#3A3A3A" stroke-width="5" transform="translate(5, 5)" stroke-miterlimit="10" x1="5" y1="5" x2="50" y2="50" stroke-linecap="round"/>
                        <line id="_x2F_" fill="none" stroke="#3A3A3A" stroke-width="5" transform="translate(5, 5)" stroke-miterlimit="10" x1="5" y1="50" x2="50" y2="5" stroke-linecap="round"/>
                    </g>
                </svg>
                <div>
                    <a href="/about-us/">About</a><br>
                    <h5>Products</h5>
                    <hr>
                    <a href="/products/fixed">• Fixed</a><br>
                    <a href="/products/removables">• Removables</a><br>
                    <a href="/products/implants">• Implants</a><br>
                    <a href="/products/services">• Additional Services</a><br>
                    <h5>Send a Case</h5>
                    <hr>
                    <a href="/send-case/new-doctor">• New Dentist</a><br>
                    <a href="/send-case/digital-case">• Digital Protocols</a><br>
                    <a href="/img/" target="_blank">• Rx Form</a><br>
                    <a href="/send-case/shipping-label">• FedEx Shipping Label</a><br>
                    <a href="/send-case/case-calendar">• Case Scheduler</a><br>
                    <hr>
                    <a href="/resources/" class="">Resources</a><br>
                    <hr>
                    <a href="/contact/" class="">Contact</a><br>
                </div>
            </div>
        </header>
        <main role="main">
            @yield('body')
        </main>
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-3">
                        <h5>Send a Case</h5>
                        <ul>
                            <li><a href="/send-case/new-doctor/">New Doctor</a></li>
                            <li><a href="/send-case/shipping-label/">Shipping Label</a></li>
                            <li><a href="/resources">Rx Form</a></li>
                            <li><a href="/send-case/digital-case/">Digital Case</a></li>
                        </ul>
                    </div>
                    <div class="col-12 col-md-3">
                        <h5>Departments</h5>
                        <ul>
                            <li><a href="/products/fixed/">Fixed</a></li>
                            <li><a href="/products/removables/">Removables</a></li>
                            <li><a href="/products/implants/">Implants</a></li>
                            <li><a href="/products/services/">Services</a></li>
                        </ul>
                    </div>
                    <div class="col-12 col-md-3">
                        <h5>Get In Touch</h5>
                        <ul>
                            <li>+1-512-689-2744</li>
                        </ul>
                    </div>
                    <div class="col-12 col-md-3">
                        <a href="/#">
                            <img src="/img/Seatac-Dental-Lab-Logo.png" alt="Seatac Dental Lab">
                        </a>
                    </div>
                </div>
            </div>
        </footer>
        <div id="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-6 d-flex" style="align-self: center;"><span>Copyright &copy; {{ date("Y") }} - <em>All Rights Reserved</em></span></div>
                    <div class="col-12 col-md-6" style="text-align: right;"><a href="https://amgci.com/?cref=1"><img src="/img/DesignedBy-AMGLogo.svg" alt="Website Design by AMG Creative" width="192" height="45" /></a></div>
                </div>
            </div>
        </div>
        
        <script src="/assets/js/script.js"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        @yield('scripts')
        
        <!-- Global site tag (gtag.js) - Google Analytics -->
        
        
        <!-- Browser Compatibility -->
        <script> 
            var $buoop = {required:{e:-0.01,i:999,f:-1,o:0,s:0,c:-1},insecure:true,unsupported:true,api:2018.09,reminder:0,reminderClosed:1,no_permanent_hide:true }; 
            function $buo_f(){ 
             var e = document.createElement("script"); 
             e.src = "//browser-update.org/update.min.js"; 
             document.body.appendChild(e);
            };
            try {document.addEventListener("DOMContentLoaded", $buo_f,false)}
            catch(e){window.attachEvent("onload", $buo_f)}
        </script>
        
    </body>
</html>
