<?php

return [
    'baseUrl' => '',
    'production' => false,
    'site_name' => 'Sky Dental Lab',
    'collections' => [
        'products' => [
            'path' => '/products/'
        ],
        'fixed' => [
            'path' => '/products/fixed/'
        ],
        'removable' => [
            'path' => '/products/removable/'
        ],
        'implants' => [
            'path' => '/products/implants/'
        ],
        'services' => [
            'path' => '/products/services/'
        ],
        'sendcase' => [
            'path' => '/send-case/'
        ],
        'about-us' => [
            'path' => '/about-us/'
        ]
    ]
];
